package ru.t1.shipilov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<ProjectDTO> getEntityClass() {
        return ProjectDTO.class;
    }

}

