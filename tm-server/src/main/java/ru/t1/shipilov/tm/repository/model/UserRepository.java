package ru.t1.shipilov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.api.repository.model.IUserRepository;
import ru.t1.shipilov.tm.model.User;

import javax.persistence.EntityManager;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull String login) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(query, getEntityClass()).setParameter("login", login).getResultStream().findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull String email) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(query, getEntityClass()).setParameter("email", email).getResultStream().findFirst().orElse(null);
    }

    @Override
    @NotNull
    public Boolean isLoginExist(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @Override
    @NotNull
    public Boolean isEmailExist(@NotNull String email) {
        return findByEmail(email) != null;
    }

}

